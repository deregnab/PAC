from subprocess import Popen, PIPE
import os, client, openssl

def encrypt_pk(plaintext,url,filename):
	file=open(filename,"w")
	file.write(client.Connection().get(url))
	file.close()

	args = ['openssl', 'pkeyutl', '-encrypt' ,'-pubin','-base64', '-inkey' + filename + '-in' + plaintext]
	if isinstance(plaintext, str):
   		plaintext = plaintext.encode('utf-8')
	pipeline = Popen(args, stdin=PIPE, stdout=PIPE, stderr=PIPE)
	stdout, stderr = pipeline.communicate(plaintext)

    # si un message d'erreur est présent sur stderr, on arrête tout
    # attention, sur stderr on récupère des bytes(), donc on convertit
	error_message = stderr.decode()
	
    # OK, openssl a envoyé le chiffré sur stdout, en base64.
    # On récupère des bytes, donc on en fait une chaine unicode
	return stdout.decode()
