import openssl as o
import json as j
connection=None
gateway="/bin/test/gateway"
session_key="debug-me"

def get(url,decode=True):
	res=connection.post_raw(gateway,o.decrypt_64(o.encrypt(str(j.dumps({"url":url,"method":"GET"})),session_key)))
	return o.decrypt(o.encrypt_base64(res),session_key).decode()

def post(url,**args):
	res=connection.post_raw(gateway,o.decrypt_64(o.encrypt(str(j.dumps({"url":url,"method":"POST","args":args})),session_key)))
	return o.decrypt(o.encrypt_base64(res),session_key).decode()

def put(url,data):
	res=connection.post_raw(gateway,o.decrypt_64(o.encrypt(str(j.dumps({"url":url,"method":"PUT","data":o.encrypt_base64(data).decode()})),session_key)))
	return o.decrypt(o.encrypt_base64(res),session_key).decode()

