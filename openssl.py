from subprocess import Popen, PIPE
import random

class OpensslError(Exception):
    pass

def encrypt(plaintext, passphrase, cipher='aes-128-cbc'):
    pass_arg = 'pass:{0}'.format(passphrase)
    args = ['openssl', 'enc', '-' + cipher, '-base64', '-pass', pass_arg, '-md', 'sha256']
    if isinstance(plaintext, str):
        plaintext = plaintext.encode('utf-8')
    pipeline = Popen(args, stdin=PIPE, stdout=PIPE, stderr=PIPE)
    stdout, stderr = pipeline.communicate(plaintext)
    error_message = stderr.decode()
    if error_message != '':
        raise OpensslError(error_message)
    return stdout.decode()

def decrypt(cryptedtext, passphrase, cipher='aes-128-cbc'):
    pass_arg = 'pass:{0}'.format(passphrase)
    args = ['openssl', 'enc','-d', '-' + cipher, '-base64', '-pass', pass_arg, '-md', 'sha256']
    if isinstance(cryptedtext, str):
        cryptedtext = cryptedtext.encode('utf-8')
    pipeline = Popen(args, stdin=PIPE, stdout=PIPE, stderr=PIPE)
    stdout, stderr = pipeline.communicate(cryptedtext)
    error_message = stderr.decode()
    if error_message != '':
        raise OpensslError(error_message)
    return stdout.decode()

def encrypt_base64(text):
	args = ['base64']
	pipeline = Popen(args, stdin=PIPE, stdout=PIPE, stderr=PIPE)
	if isinstance(text, str):
		text = text.encode('utf-8')
	stdout, stderr = pipeline.communicate(text)
	error_message = stderr.decode()
	if error_message != '':
		raise OpensslError(error_message)
	return stdout

def decrypt_base64(text):
	args = ['base64', '-d']
	pipeline = Popen(args, stdin=PIPE, stdout=PIPE, stderr=PIPE)
	if isinstance(text, str):
		text = text.encode('utf-8')
	stdout, stderr = pipeline.communicate(text)
	error_message = stderr.decode()
	if error_message != '':
		raise OpensslError(error_message)
	return stdout

def keygen(user,size="2048"):
	args = ['openssl', 'genpkey', '-algorithm', 'RSA', '-pkeyopt', 'rsa_keygen_bits:'+str(size)]
	pipeline = Popen(args, stdin=PIPE, stdout=PIPE, stderr=PIPE)
	stdout, stderr = pipeline.communicate()
	tmp_file = open(str(user),"wb")
	tmp_file.write(stdout)
	tmp_file.close()
	args = ['openssl', 'pkey', '-in', str(user), '-pubout']
	pipeline = Popen(args, stdin=PIPE, stdout=PIPE, stderr=PIPE)
	stdout, stderr = pipeline.communicate("")
	error_message = stderr.decode()
	if error_message != '':
		raise OpensslError(error_message)
	tmp_file = open(str(user)+".pub","wb")
	tmp_file.write(stdout)
	tmp_file.close()

def encrypt_pub(plaintext, pub):
	args = ['openssl', 'pkeyutl', '-encrypt', '-pubin', '-inkey', pub]
	if isinstance(plaintext, str):
		plaintext = plaintext.encode('utf-8')
	pipeline = Popen(args, stdin=PIPE, stdout=PIPE, stderr=PIPE)
	stdout, stderr = pipeline.communicate(plaintext)
	error_message = stderr.decode()
	if error_message != '':
		raise OpensslError(error_message)
	return encrypt_base64(stdout).decode()

def decrypt_pub(cryptedtext, privatekey='private_key'):
	tmp= decrypt_base64(cryptedtext)
	args = ['openssl','pkeyutl','-decrypt','-inkey',privatekey]
	pipeline2 = Popen(args, stdin=PIPE, stdout=PIPE, stderr=PIPE)
	stdout, stderr = pipeline2.communicate(tmp)
	return stdout.decode()

def encrypt_hybrid(plaintext, pub):
    key = "a"+str(random.getrandbits(256))
    ret1 = encrypt_pub(key,pub)
    ret2 = encrypt(plaintext,key)
    return {"session_key":ret1,"payload":ret2}

def decrypt_hybrid(cryptedtext, privatekey='private_key', cipher='aes-128-cbc'):
    if(isInstance(cryptedtext,str)):
        cryptedtext=json.loads(cryptedtext)
    key = decrypt_pub(cryptedtext["session_key"],privatekey)
    return decrypt(cryptedtext["payload"],key,cipher)

def sign(plaintext,privatekey='private_key'):
	args = ['openssl', 'dgst', '-sha256', '-sign', privatekey]
	if isinstance(plaintext, str):
		plaintext = plaintext.encode('utf-8')
	pipeline = Popen(args, stdin=PIPE, stdout=PIPE, stderr=PIPE)
	stdout, stderr = pipeline.communicate(plaintext)
	error_message = stderr.decode()
	if error_message != '':
		raise OpensslError(error_message)
	return encrypt_base64(stdout).decode()

def verify_sign(sign,plaintext,publickey='public_key'):
	sign = decrypt_base64(sign)
	tmp_file = open("verify_sign_tmp","wb")
	tmp_file.write(sign)
	tmp_file.close()
	args = ['openssl', 'dgst', '-sha256', '-verify', publickey, '-signature', "verify_sign_tmp"]
	if isinstance(plaintext, str):
		plaintext = plaintext.encode('utf-8')
	pipeline = Popen(args, stdin=PIPE, stdout=PIPE, stderr=PIPE)
	stdout, stderr = pipeline.communicate(plaintext)
	error_message = stderr.decode()
	if error_message != '':
		raise OpensslError(error_message)
	return stdout.decode()

def one_time(a,b):
    a=decrypt_base64(a)
    b=decrypt_base64(b)
    ab=tmp(a,b)
    bPart=b'0'*len(ab)
    aPart=tmp(bPart,ab)
    print(aPart)
def tmp(a,b):
    p=[]
    for a,b in zip(a,b):
        p.append(bytes([a ^ b]))
    return b''.join(p)
